package me.sgayazov.newsapp;

import android.app.Application;

import io.realm.Realm;
import me.sgayazov.newsapp.inject.component.AppComponent;
import me.sgayazov.newsapp.inject.component.DaggerAppComponent;
import me.sgayazov.newsapp.inject.module.AppModule;

public class NewsApp extends Application {

    private AppComponent appComponent;

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        appComponent = initDagger(this);
    }

    protected AppComponent initDagger(NewsApp application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }
}
