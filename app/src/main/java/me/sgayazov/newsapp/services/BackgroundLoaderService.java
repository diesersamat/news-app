package me.sgayazov.newsapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import javax.inject.Inject;

import me.sgayazov.newsapp.NewsApp;
import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.inject.component.DaggerServiceComponent;
import me.sgayazov.newsapp.presentation.vo.NewsVO;
import rx.Subscriber;

public class BackgroundLoaderService extends IntentService {

    @Inject
    Interactor interactor;

    public BackgroundLoaderService() {
        super(BackgroundLoaderService.class.getName());
    }


    @Override
    public void onCreate() {
        super.onCreate();
        DaggerServiceComponent.builder()
                .appComponent(((NewsApp) getApplication()).getAppComponent())
                .build()
                .inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        interactor
                .getNews(Interactor.REQUEST_TYPE_UPDATE_FROM_API_AND_SAVE)
                .subscribe(new Subscriber<NewsVO>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("BCGK", e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(NewsVO newsVO) {
                        Log.e("BCGK", newsVO.toString());
                    }
                });
    }
}
