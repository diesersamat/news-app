package me.sgayazov.newsapp.presentation.presenters;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.presentation.vo.NewsVO;
import me.sgayazov.newsapp.view.interfaces.NewsListView;
import rx.Subscriber;

public class NewsListPresenter extends BasePresenter {

    @Inject
    NewsListPresenter(NewsListView view, Interactor interactor) {
        super(view, interactor);
    }

    @Override
    public void onStartLoad() {
        getView().onLoadStarted();
        getInteractor().getNews(Interactor.REQUEST_TYPE_UPDATE_FROM_API_IF_DB_EMPTY).subscribe(getSubscriber());
    }

    @Override
    protected NewsListView getView() {
        return (NewsListView) super.getView();
    }

    @NonNull
    private Subscriber<NewsVO> getSubscriber() {
        return new BasePresenter.SimpleSubscriber<NewsVO>() {

            @Override
            public void onError(Throwable e) {
                getView().onDataLoadFailed();
            }

            @Override
            public void onNext(NewsVO newsVO) {
                getView().onDataLoaded(newsVO);
            }
        };
    }
}
