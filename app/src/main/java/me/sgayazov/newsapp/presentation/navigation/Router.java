package me.sgayazov.newsapp.presentation.navigation;

import android.content.Context;

import me.sgayazov.newsapp.view.activity.AboutActivity;
import me.sgayazov.newsapp.view.activity.DetailedNewsActivity;
import me.sgayazov.newsapp.view.activity.NewsListActivity;

public class Router {

    public void navigateToNewsList(Context context) {
        context.startActivity(NewsListActivity.getNavigateIntent(context));
    }

    public void navigateToNewsDetailed(Context context, String url) {
        context.startActivity(DetailedNewsActivity.getNavigateIntent(context, url));
    }

    public void navigateToDeveloperInfo(Context context) {
        context.startActivity(AboutActivity.getNavigateIntent(context));
    }
}
