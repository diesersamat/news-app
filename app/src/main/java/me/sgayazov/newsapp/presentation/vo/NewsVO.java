package me.sgayazov.newsapp.presentation.vo;

import java.util.List;

public class NewsVO {

    List<NewsItemVO> newsItemVOs;

    public List<NewsItemVO> getNewsItemVOs() {
        return newsItemVOs;
    }

    public void setNewsItemVOs(List<NewsItemVO> newsItemVOs) {
        this.newsItemVOs = newsItemVOs;
    }
}
