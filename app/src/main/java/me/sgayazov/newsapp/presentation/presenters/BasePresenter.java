package me.sgayazov.newsapp.presentation.presenters;

import android.util.Log;

import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.view.interfaces.BaseView;
import rx.Subscriber;

public abstract class BasePresenter {

    private final Interactor interactor;

    private final BaseView view;

    BasePresenter(BaseView view, Interactor interactor) {
        this.interactor = interactor;
        this.view = view;
    }

    public abstract void onStartLoad();

    protected Interactor getInteractor() {
        return interactor;
    }

    protected BaseView getView() {
        return view;
    }

    class SimpleSubscriber<T> extends Subscriber<T> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            Log.w(getClass().getSimpleName(), e.toString());
            e.printStackTrace();
        }

        @Override
        public void onNext(T t) {

        }
    }
}
