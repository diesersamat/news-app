package me.sgayazov.newsapp.presentation.presenters;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.presentation.vo.NewsVO;
import me.sgayazov.newsapp.view.interfaces.SplashView;
import rx.Subscriber;

public class SplashPresenter extends BasePresenter {

    @Inject
    SplashPresenter(SplashView view, Interactor interactor) {
        super(view, interactor);
    }

    @Override
    public void onStartLoad() {
        getView().onLoadStarted();
        getInteractor().getNews(Interactor.REQUEST_TYPE_UPDATE_FROM_API_IF_DB_EMPTY).subscribe(getSubscriber());
        getInteractor().runService();
    }

    public String getDrawableForPhone(String s) {
        if (s.startsWith("8") || s.startsWith("+7")) {
            return "\uD83C\uDDF7\uD83C\uDDFA";
        }

        if (s.startsWith("+375")) {
            return "\uD83C\uDDE7\uD83C\uDDFE";
        }

        if (s.startsWith("+380")) {
            return "\uD83C\uDDFA\uD83C\uDDE6";
        }

        if (s.startsWith("+992")) {
            return "\uD83C\uDDF9\uD83C\uDDEF";
        }
        return "";
    }

    @Override
    protected SplashView getView() {
        return (SplashView) super.getView();
    }

    @NonNull
    private Subscriber<NewsVO> getSubscriber() {
        return new SimpleSubscriber<NewsVO>() {

            @Override
            public void onError(Throwable e) {
                getView().onDataLoadFailed();
            }

            @Override
            public void onNext(NewsVO newsVO) {
                getView().onDataLoaded();
            }
        };
    }
}
