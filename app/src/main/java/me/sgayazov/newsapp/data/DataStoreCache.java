package me.sgayazov.newsapp.data;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import me.sgayazov.newsapp.data.dto.NewsDTO;
import rx.Observable;

public class DataStoreCache {

    @Inject
    public DataStoreCache() {
    }

    public Observable<NewsDTO> getNews() {
        RealmResults<NewsDTO> all = getRealm().where(NewsDTO.class).findAll();
        return Observable.from(getRealm().copyFromRealm(all));
    }

    public void saveNews(NewsDTO newsDTO) {
        getRealm().executeTransaction(realm ->
                realm.insertOrUpdate(newsDTO));
    }

    private Realm getRealm() {
        return Realm.getDefaultInstance();
    }
}
