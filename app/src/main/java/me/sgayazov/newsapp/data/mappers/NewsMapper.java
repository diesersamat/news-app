package me.sgayazov.newsapp.data.mappers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import me.sgayazov.newsapp.data.dto.ItemDTO;
import me.sgayazov.newsapp.data.dto.NewsDTO;
import me.sgayazov.newsapp.presentation.vo.NewsItemVO;
import me.sgayazov.newsapp.presentation.vo.NewsVO;

public class NewsMapper {
    private NewsMapper() {
    }

    public static NewsVO mapNews(NewsDTO newsDTO) {
        NewsVO newsVO = new NewsVO();
        List<NewsItemVO> newsItemVOs = new ArrayList<>();
        newsVO.setNewsItemVOs(newsItemVOs);
        for (ItemDTO item : newsDTO.getChannel().getItem()) {
            NewsItemVO newsItem = new NewsItemVO();
            newsItem.setTitle(item.getTitle());
            newsItem.setDescription(item.getDescription());
            try {
                DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
                Date result = df.parse(item.getPubDate());
                newsItem.setPublicationDate(result);
            } catch (ParseException e) {
                Logger.getGlobal().log(Level.WARNING, e.getLocalizedMessage());
                newsItem.setPublicationDate(new Date());
            }
            newsItem.setURL(item.getLink());
            newsItemVOs.add(newsItem);
        }
        return newsVO;
    }
}
