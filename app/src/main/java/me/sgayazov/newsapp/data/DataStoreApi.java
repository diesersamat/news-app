package me.sgayazov.newsapp.data;

import javax.inject.Inject;

import me.sgayazov.newsapp.data.dto.NewsDTO;
import me.sgayazov.newsapp.data.net.ApiInterface;
import rx.Observable;

public class DataStoreApi {

    private final ApiInterface apiInterface;

    @Inject
    public DataStoreApi(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    public Observable<NewsDTO> getNews() {
        return apiInterface.getNews();
    }
}
