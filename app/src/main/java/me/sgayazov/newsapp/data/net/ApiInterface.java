package me.sgayazov.newsapp.data.net;

import me.sgayazov.newsapp.data.dto.NewsDTO;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiInterface {

    @GET("_rss.html?subtype=1&category=2&city=21")
    Observable<NewsDTO> getNews();
}