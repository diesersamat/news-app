package me.sgayazov.newsapp.data;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;

import me.sgayazov.newsapp.data.mappers.NewsMapper;
import me.sgayazov.newsapp.data.provider.BackgroundServiceProvider;
import me.sgayazov.newsapp.data.provider.NewsDataProvider;
import me.sgayazov.newsapp.presentation.vo.NewsVO;
import rx.Observable;

public class Interactor {
    public static final int REQUEST_TYPE_UPDATE_FROM_API_AND_SAVE = 0;
    public static final int REQUEST_TYPE_UPDATE_FROM_API_IF_DB_EMPTY = 1;

    NewsDataProvider newsDataProvider;

    BackgroundServiceProvider backgroundServiceProvider;

    @Inject
    public Interactor(NewsDataProvider newsDataProvider, BackgroundServiceProvider serviceProvider) {
        this.newsDataProvider = newsDataProvider;
        this.backgroundServiceProvider = serviceProvider;
    }

    public Observable<NewsVO> getNews(@RequestType int requestType) {
        return newsDataProvider.getNews(requestType).map(NewsMapper::mapNews);
    }

    public void runService() {
        backgroundServiceProvider.startBackgroundService();
    }

    public void stopService() {
        backgroundServiceProvider.cancelBackgroundService();
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({REQUEST_TYPE_UPDATE_FROM_API_AND_SAVE,
            REQUEST_TYPE_UPDATE_FROM_API_IF_DB_EMPTY})
    public @interface RequestType {
    }

}
