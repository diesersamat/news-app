package me.sgayazov.newsapp.data.provider;

import javax.inject.Inject;

import me.sgayazov.newsapp.data.DataStoreApi;
import me.sgayazov.newsapp.data.DataStoreCache;
import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.data.dto.NewsDTO;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NewsDataProvider {

    private final DataStoreApi dataStoreApi;
    private final DataStoreCache dataStoreCache;

    @Inject
    public NewsDataProvider(DataStoreApi dataStoreApi, DataStoreCache dataStoreCache) {
        this.dataStoreApi = dataStoreApi;
        this.dataStoreCache = dataStoreCache;
    }

    public Observable<NewsDTO> getNews(@Interactor.RequestType int requestType) {
        if (requestType == Interactor.REQUEST_TYPE_UPDATE_FROM_API_IF_DB_EMPTY) {
            return Observable.defer(dataStoreCache::getNews)
                    .switchIfEmpty(dataStoreApi.getNews()
                            .doOnNext(dataStoreCache::saveNews))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        } else if (requestType == Interactor.REQUEST_TYPE_UPDATE_FROM_API_AND_SAVE) {
            return dataStoreApi
                    .getNews()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(dataStoreCache::saveNews);
        } else {
            throw new IllegalStateException("Wrong request type!");
        }
    }
}
