package me.sgayazov.newsapp.data.provider;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import me.sgayazov.newsapp.brreceivers.UpdateBroadcastReceiver;

public class BackgroundServiceProvider {

    private final AlarmManager alarmManager;
    private final Context context;

    public BackgroundServiceProvider(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void startBackgroundService() {
        Intent intent = new Intent(context, UpdateBroadcastReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(context, UpdateBroadcastReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis();
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, 5 * 60 * 1000, pIntent);
    }

    public void cancelBackgroundService() {
        Intent intent = new Intent(context, UpdateBroadcastReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(context, UpdateBroadcastReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pIntent);
    }

}
