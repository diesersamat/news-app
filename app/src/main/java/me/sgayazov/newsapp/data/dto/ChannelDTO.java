package me.sgayazov.newsapp.data.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import io.realm.RealmList;
import io.realm.RealmObject;

@Root(name = "channel")
public class ChannelDTO extends RealmObject {

    @Element(name = "pubDate")
    private String pubDate;
    @Element(name = "title")
    private String title;
    @Element(name = "managingEditor")
    private String managingEditor;
    @Element(name = "description")
    private String description;
    @Element(name = "docs")
    private String docs;
    @Element(name = "link")
    private String link;
    @ElementList(inline=true, name="item")
    private RealmList<ItemDTO> item;
    @Element(name = "language")
    private String language;
    @Element(name = "webMaster")
    private String webMaster;

    public ChannelDTO() {
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getManagingEditor() {
        return managingEditor;
    }

    public void setManagingEditor(String managingEditor) {
        this.managingEditor = managingEditor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocs() {
        return docs;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public RealmList<ItemDTO> getItem() {
        return item;
    }

    public void setItem(RealmList<ItemDTO> item) {
        this.item = item;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getWebMaster() {
        return webMaster;
    }

    public void setWebMaster(String webMaster) {
        this.webMaster = webMaster;
    }
}
