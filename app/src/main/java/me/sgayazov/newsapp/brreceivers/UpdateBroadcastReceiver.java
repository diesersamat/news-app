package me.sgayazov.newsapp.brreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import me.sgayazov.newsapp.services.BackgroundLoaderService;

public class UpdateBroadcastReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, BackgroundLoaderService.class));
    }
}
