package me.sgayazov.newsapp.inject.component;

import dagger.BindsInstance;
import dagger.Component;
import me.sgayazov.newsapp.inject.PerActivity;
import me.sgayazov.newsapp.view.activity.SplashActivity;
import me.sgayazov.newsapp.view.interfaces.SplashView;

@Component(dependencies = AppComponent.class)
@PerActivity
public interface SplashComponent {
    void inject(SplashActivity in);

    @Component.Builder
    interface Builder {
        Builder appComponent(AppComponent appComponent);

        @BindsInstance
        Builder view(SplashView view);

        SplashComponent build();
    }
}
