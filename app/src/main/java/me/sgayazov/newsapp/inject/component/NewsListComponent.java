package me.sgayazov.newsapp.inject.component;

import dagger.BindsInstance;
import dagger.Component;
import me.sgayazov.newsapp.inject.PerActivity;
import me.sgayazov.newsapp.view.activity.NewsListActivity;
import me.sgayazov.newsapp.view.interfaces.NewsListView;

@Component(dependencies = AppComponent.class)
@PerActivity
public interface NewsListComponent {
    void inject(NewsListActivity in);

    @Component.Builder
    interface Builder {
        Builder appComponent(AppComponent appComponent);

        @BindsInstance
        Builder view(NewsListView view);

        NewsListComponent build();
    }
}
