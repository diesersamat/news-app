package me.sgayazov.newsapp.inject.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sgayazov.newsapp.data.DataStoreApi;
import me.sgayazov.newsapp.data.DataStoreCache;
import me.sgayazov.newsapp.data.provider.BackgroundServiceProvider;
import me.sgayazov.newsapp.data.provider.NewsDataProvider;
import me.sgayazov.newsapp.presentation.navigation.Router;

@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    Router provideRouter() {
        return new Router();
    }

    @Provides
    @Singleton
    BackgroundServiceProvider provideBackgroundServiceProvider(Context context) {
        return new BackgroundServiceProvider(context);
    }

    @Provides
    @Singleton
    NewsDataProvider provideNewsDataProvider(DataStoreApi dataStoreApi, DataStoreCache dataStoreCache) {
        return new NewsDataProvider(dataStoreApi, dataStoreCache);
    }
}
