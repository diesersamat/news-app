package me.sgayazov.newsapp.inject.component;

import dagger.Component;
import me.sgayazov.newsapp.inject.PerActivity;
import me.sgayazov.newsapp.services.BackgroundLoaderService;

@Component(dependencies = AppComponent.class)
@PerActivity
public interface ServiceComponent {
    void inject(BackgroundLoaderService in);

    @Component.Builder
    interface Builder {
        Builder appComponent(AppComponent appComponent);

        ServiceComponent build();
    }
}
