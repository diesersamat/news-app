package me.sgayazov.newsapp.inject.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import me.sgayazov.newsapp.NewsApp;
import me.sgayazov.newsapp.data.Interactor;
import me.sgayazov.newsapp.inject.module.ApiModule;
import me.sgayazov.newsapp.inject.module.AppModule;
import me.sgayazov.newsapp.presentation.navigation.Router;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    void inject(NewsApp in);

    void inject(Interactor in);

    Context getContext();

    Interactor getInteractor();

    Router getRouter();
}
