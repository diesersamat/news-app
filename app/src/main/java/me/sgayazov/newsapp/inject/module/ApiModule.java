package me.sgayazov.newsapp.inject.module;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sgayazov.newsapp.BuildConfig;
import me.sgayazov.newsapp.data.net.ApiInterface;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class ApiModule {
    private static final String API_ROOT = BuildConfig.API_URL;

    private static OkHttpClient buildHttpClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        return builder.build();
    }

    @Provides
    @Singleton
    ApiInterface provideApi(Context context) {
        final OkHttpClient okHttpClient = buildHttpClient(context);

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .baseUrl(API_ROOT)
                .client(okHttpClient)
                .build()
                .create(ApiInterface.class);
    }
}