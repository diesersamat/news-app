package me.sgayazov.newsapp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.sgayazov.newsapp.R;
import me.sgayazov.newsapp.presentation.vo.NewsItemVO;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder> {
    private final LayoutInflater layoutInflater;
    private List<NewsItemVO> list;
    private OnNewsClickListener onNewsClickListener;

    @Inject
    public NewsListAdapter(Context context) {
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        list = Collections.emptyList();
    }


    @Override
    public int getItemCount() {
        return (this.list != null) ? this.list.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void setList(List<NewsItemVO> list) {
        if (list == null) {
            return;
        }
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.news_item, parent, false);
        return new NewsItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsItemViewHolder holder, int position) {
        holder.dateAdded.setText(list.get(position).getPublicationDate().toString());
        holder.description.setText(Html.fromHtml(list.get(position).getDescription()));
        holder.title.setText(Html.fromHtml(list.get(position).getTitle()));
        holder.itemView.setOnClickListener(v -> {
            if (onNewsClickListener != null) {
                onNewsClickListener.onNewsClick(list.get(position).getURL());
            }
        });
    }

    public void setOnNewsClickListener(OnNewsClickListener onNewsClickListener) {
        this.onNewsClickListener = onNewsClickListener;
    }

    public interface OnNewsClickListener {
        void onNewsClick(String url);
    }

    class NewsItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.date_added)
        TextView dateAdded;


        public NewsItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
