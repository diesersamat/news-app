package me.sgayazov.newsapp.view.interfaces;

public interface SplashView extends BaseView {
    void onDataLoaded();
}
