package me.sgayazov.newsapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.sgayazov.newsapp.R;

public class DetailedNewsActivity extends AppCompatActivity {

    private static final String URL = "URL";

    @BindView(R.id.webview)
    WebView webView;


    public static Intent getNavigateIntent(Context context, String url) {
        Intent intent = new Intent(context, DetailedNewsActivity.class);
        intent.putExtra(URL, url);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.news_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_news);
        ButterKnife.bind(this);
        setTitle(getString(R.string.news_title));
    }

    @Override
    protected void onResume() {
        super.onResume();
        String url = getIntent().getStringExtra(URL);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }

    private void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getIntent().getStringExtra(URL));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
