package me.sgayazov.newsapp.view.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.sgayazov.newsapp.NewsApp;
import me.sgayazov.newsapp.R;
import me.sgayazov.newsapp.inject.component.DaggerSplashComponent;
import me.sgayazov.newsapp.presentation.presenters.SplashPresenter;
import me.sgayazov.newsapp.view.interfaces.SplashView;

public class SplashActivity extends BaseActivity implements SplashView {
    @Inject
    SplashPresenter presenter;

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.error_layout)
    View error;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.sign_in_layout)
    View signInLayout;
    @BindView(R.id.flag)
    TextView flag;

    @Override
    public void onDataLoaded() {
        progressBar.setVisibility(View.GONE);
        signInLayout.setVisibility(View.VISIBLE);
        phone.addTextChangedListener(getPhoneWatcher());
    }

    @Override
    public void onDataLoadFailed() {
        error.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public SplashPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onLoadStarted() {
        error.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setTitle(getString(R.string.loading_news_splash));
    }

    @Override
    protected void additionalCreateOperations() {
        DaggerSplashComponent.builder()
                .appComponent(((NewsApp) getApplication()).getAppComponent())
                .view(this)
                .build()
                .inject(this);
    }

    @NonNull
    private TextWatcher getPhoneWatcher() {
        return new TextWatcher() {
            boolean ignoreChange = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (ignoreChange) {
                    ignoreChange = false;
                } else {
                    ignoreChange = true;
                    String formatNumber = getFormattedNumber(s.toString());
                    phone.setText(formatNumber == null ? s : formatNumber);
                    phone.setSelection(phone.getText().toString().length());
                    flag.setText(getPresenter().getDrawableForPhone(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    private String getFormattedNumber(String numb) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return PhoneNumberUtils.formatNumber(numb, Locale.getDefault().getCountry());
        } else {
            //Deprecated method
            return PhoneNumberUtils.formatNumber(numb);
        }
    }

    @OnClick(R.id.retry_button)
    void onRetryClick() {
        getPresenter().onStartLoad();
    }

    @OnClick(R.id.login_button)
    void onLoginClick() {
        getRouter().navigateToNewsList(this);
        finish();
    }
}
