package me.sgayazov.newsapp.view.interfaces;

public interface BaseView {
    void onDataLoadFailed();

    void onLoadStarted();
}
