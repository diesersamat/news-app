package me.sgayazov.newsapp.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import me.sgayazov.newsapp.presentation.navigation.Router;
import me.sgayazov.newsapp.presentation.presenters.BasePresenter;
import me.sgayazov.newsapp.view.interfaces.BaseView;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    public static final String SKU = "SKU";

    @Inject
    Router router;

    protected Router getRouter() {
        return router;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        additionalCreateOperations();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().onStartLoad();
    }

    protected abstract BasePresenter getPresenter();

    protected abstract void additionalCreateOperations();
}
