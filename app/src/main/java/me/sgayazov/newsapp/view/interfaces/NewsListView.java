package me.sgayazov.newsapp.view.interfaces;

import me.sgayazov.newsapp.presentation.vo.NewsVO;

public interface NewsListView extends BaseView {
    void onDataLoaded(NewsVO newsVO);
}
