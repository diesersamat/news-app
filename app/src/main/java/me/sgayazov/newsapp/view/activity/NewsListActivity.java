package me.sgayazov.newsapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.sgayazov.newsapp.NewsApp;
import me.sgayazov.newsapp.R;
import me.sgayazov.newsapp.inject.component.DaggerNewsListComponent;
import me.sgayazov.newsapp.presentation.presenters.NewsListPresenter;
import me.sgayazov.newsapp.presentation.vo.NewsVO;
import me.sgayazov.newsapp.view.adapters.NewsListAdapter;
import me.sgayazov.newsapp.view.interfaces.NewsListView;

public class NewsListActivity extends BaseActivity implements NewsListView {

    @Inject
    NewsListPresenter presenter;
    @Inject
    NewsListAdapter adapter;

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.error_layout)
    View error;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static Intent getNavigateIntent(Context context) {
        return new Intent(context, NewsListActivity.class);
    }

    @Override
    public void onDataLoadFailed() {
        error.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onLoadStarted() {
        error.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onDataLoaded(NewsVO newsVO) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.setList(newsVO.getNewsItemVOs());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_about_developer) {
            getRouter().navigateToDeveloperInfo(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ButterKnife.bind(this);
        setTitle(getString(R.string.news_list_title));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.setOnNewsClickListener(url -> getRouter().navigateToNewsDetailed(this, url));
    }

    @Override
    protected NewsListPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void additionalCreateOperations() {
        DaggerNewsListComponent.builder()
                .appComponent(((NewsApp) getApplication()).getAppComponent())
                .view(this)
                .build()
                .inject(this);
    }

    @OnClick(R.id.retry_button)
    void onRetryClick() {
        getPresenter().onStartLoad();
    }
}
